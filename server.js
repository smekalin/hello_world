"use strict";

const express = require("express");
var sendpulse = require("sendpulse-api");

// константы
const port = 80;
const host = "0.0.0.0";

// приложение
const app = express();
app.get("/", (req, res) => {
  res.send("HELLO WORLD master");
});

app.listen(port, host);
console.log(`running on http://${host}:${port}`);
